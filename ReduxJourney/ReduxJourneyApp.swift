//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import SwiftUI

@main
struct ReduxJourneyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
