//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift

struct Actions {
    typealias Action = ReSwift.Action
    
    struct Init: Action { }
}
