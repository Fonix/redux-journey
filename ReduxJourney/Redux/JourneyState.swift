//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift

public struct JourneyState: Equatable {
    
    public var configuration: Journey.Configuration
    
    public var contacts = Journey.ContactState()
}
