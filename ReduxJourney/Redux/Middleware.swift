//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift

extension Journey {
    
    class SagaMiddleware {
        
        /**
         Add any new middleware layers to this array that will be added to the redux store
         
         # Note #
         order is significant but you should ideally make your middleware independent
         of other middleware thus making the order of the middleware irrelevant
         */
        lazy var middlewareLayers: [Middleware<JourneyState>] = [actionLogger, middleware]
        
        /**
         The list of sagas for this Journey.
         
         Register your sagas here for them to be notified of fired actions.
         */
        private lazy var sagas: [Saga] = [
            Journey.ContactSaga()
        ]

        // MARK: - Middleware

        /**
         Middleware that will pass any actions received to the sagas that are registered
         in the `registerSagas` method
         */
        private lazy var middleware: Middleware<JourneyState> = { dispatch, state in
            return { next in
                return { [unowned self] action in
                    
                    next(action) // Sagas should run after reducers so the state is updated before running the saga
                    
                    guard let state = state() else {
                        fatalError("Store should have state, something has gone terribly wrong")
                    }
                    
#if TEST
                    if type(of: action.self) != ReSwiftInit.self { // ignore ReSwifts init action for the history
                        actionHistory.append(action)
                    }
                    
                    stateHistory.append(state)
#endif
                    
                    for saga in sagas {
                        waitFor {
#if TEST
                            // run dispatch synchronously on same thread when testing
                            await saga.actionReceived(action, state: state, dispatch: dispatch)
#else
                            await saga.actionReceived(action, state: state, dispatch: { action in
                                // Dispatching must always occur on main thread because UI updates may occur
                                DispatchQueue.main.async {
                                    dispatch(action)
                                }
                            })
#endif
                        }
                    }
                    

                }
            }
        }
        
        // MARK: - Testing

        /**
         A middleware that will just log each action that is sent to the console and
         also keep a history of the actions that were sent (useful for testing)
         this should be removed in production code
         */
        private lazy var actionLogger: Middleware<JourneyState> = { _, state in
            return { next in
                return { [unowned self] action in

                    #if DEBUG
                    let sanitisedAction = String(reflecting: action)
                        .replacingOccurrences(of: "ReduxJourney.", with: "")
                        .replacingOccurrences(of: "Actions.", with: "")
                    
                    print("\nAction> \(sanitisedAction))")
                    #endif
                    
                    next(action)
                }
            }
        }
        
        // Useful for testing what actions were fired
        var actionHistory: [Action] = []
        var stateHistory: [JourneyState] = []
        
        // Turn on or off async saga's, useful for testing
        func waitFor(_ f: @escaping () async -> ()) {
            let sema = DispatchSemaphore(value: 0)
            Task(priority: .background) {
                await f()
                #if TEST
                sema.signal()
                #endif
            }
            #if TEST
            sema.wait()
            #endif
        }
    }
}

/**
 Conform to this protocol when creating a new saga.
 Register your saga inside the saga middleware layer so that it will get called.
 */
protocol Saga {
    func actionReceived(_ action: Action, state: JourneyState, dispatch: DispatchFunction) async
}
