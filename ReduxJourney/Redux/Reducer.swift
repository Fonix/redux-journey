//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift

extension Journey {
    
    static func mainReducer(_ action: Action,
                            state: JourneyState?) -> JourneyState {
        JourneyState(
            configuration: state?.configuration ?? Journey.Configuration(),
            contacts: contactReducer(action: action, state: state?.contacts)
        )
    }
}
