//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift
import SwiftUI

final public class ViewModel: ObservableObject, StoreSubscriber {
    
    private let store: Store<JourneyState>
    
    @Published
    public var state: JourneyState
    public let dispatch: (Action) -> Void
    
    init(store: Store<JourneyState>) {
        self.store = store
        self.state = store.state
        self.dispatch = store.dispatch
        
        store.subscribe(self)
    }
    
    public func newState(state: JourneyState) {
        self.state = state
    }
}

public protocol JourneyUI: View {
    
    var viewModel: ViewModel { get }
}

func viewModelForPreviews(state: JourneyState? = JourneyState(configuration: Journey.Configuration())) -> ViewModel {
    var state = state
    state?.contacts.items = [
        Contact(name: "John", email: "john@email.com", amount: "$3.50"),
        Contact(name: "Blake", email: "blake@email.com", amount: "$400.00")
    ]
    return ViewModel(store: Store<JourneyState>(reducer: Journey.mainReducer, state: state))
}
