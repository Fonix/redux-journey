//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import SwiftUI
import BackbaseDesignSystem

struct EmptyStateView: UIViewRepresentable {
    
    func makeUIView(context: Context) -> StateView {
        let view = StateView(configuration: NoResultsStateViewConfiguration(title: "No Contacts", subtitle: "Add a contact below"))
        
        return view
    }
    
    func updateUIView(_ uiView: StateView, context: Context) {
        
    }
}
