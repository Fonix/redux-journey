//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import SwiftUI
import ReSwift

struct ListView: JourneyUI {
    
    // External state
    @ObservedObject
    var viewModel: ViewModel
    var state: Journey.ContactState {
        viewModel.state.contacts
    }
    
    // Internal state
    @State var textInput: String = ""
    
    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            if state.empty {
                Spacer()
                EmptyStateView().frame(width: 150, height: 220, alignment: .center)
            } else {
                ForEach(state.items, id: \.self) { item in
                    ListViewItem(viewModel: viewModel, contact: item)
                }
                if state.isLoading {
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                }
            }
            Spacer()
            HStack {
                TextField("Name", text: $textInput) { changed in
                    addContact(textInput)
                }
                Button("Add") {
                    addContact(textInput)
                }
            }
        }
        .padding(EdgeInsets(top: 16,
                             leading: 16,
                             bottom: 16,
                             trailing: 16))
        .navigationTitle("Contact List")
    }
    
    func addContact(_ name: String) {
        guard !name.isEmpty else {
            return
        }
        let contact = Contact(name: name,
                              email: "\(name)@email.com",
                              amount: "$0.00")
        
        viewModel.dispatch(Actions.Contacts.Add(contact: contact))
        
        textInput = ""
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView(viewModel: viewModelForPreviews())
    }
}
