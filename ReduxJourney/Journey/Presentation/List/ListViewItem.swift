//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import SwiftUI
import ReSwift
import BackbaseDesignSystem

struct ListViewItem: View {
    
    // External state
    @ObservedObject
    var viewModel: ViewModel
    var contact: Contact
    
    var body: some View {
        NavigationLink(destination: DetailsView(viewModel: viewModel,
                                                contact: contact)){
            HStack(alignment: .top, spacing: 8) {
                VStack(alignment: .leading, spacing: 4) {
                    Text(contact.name)
                    Text(contact.email).foregroundColor(.gray)
                }
                Spacer()
                VStack(alignment: .trailing, spacing: 4) {
                    Text(contact.amount)
                    Button (action: {
                        deleteContact()
                    }) {
                        Image(DesignSystem.Assets.icDelete, bundle: .design)
                            .foregroundColor(.red)
                    }
                }
            }
        }.buttonStyle(PlainButtonStyle())
    }
    
    func deleteContact() {
        viewModel.dispatch(Actions.Contacts.Delete(contact: contact))
    }
}

struct ListViewItem_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = viewModelForPreviews()
        ListViewItem(viewModel: viewModel, contact: viewModel.state.contacts.items[0])
    }
}
