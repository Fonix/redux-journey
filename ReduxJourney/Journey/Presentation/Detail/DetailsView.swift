//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import SwiftUI

struct DetailsView: View {
    
    // External state
    @ObservedObject
    var viewModel: ViewModel
    var state: Journey.ContactState {
        viewModel.state.contacts
    }
    var contact: Contact
    
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        VStack {
            Text(contact.name)
            Text(contact.email)
            Text(contact.amount)
            Spacer().frame(width: 0, height: 20, alignment: .center)
            Button("Delete") {
                deleteContact()
            }.foregroundColor(.red)
        }
        .navigationTitle("Details")
    }
    
    func deleteContact() {
        viewModel.dispatch(Actions.Contacts.Delete(contact: contact))
        presentation.wrappedValue.dismiss()
    }
}
