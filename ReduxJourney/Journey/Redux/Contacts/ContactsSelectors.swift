//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation

extension Journey.ContactState {
    
    var empty: Bool {
        return items.count == 0 && !isLoading
    }
}
