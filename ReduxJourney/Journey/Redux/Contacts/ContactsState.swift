//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation

extension Journey {
    
    public struct ContactState: Equatable {
        
        var items: [Contact] = []
        var isLoading = false
    }
}
