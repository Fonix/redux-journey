//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift

extension Journey {
    
    static func contactReducer(action: Action, state: ContactState?) -> ContactState {
        var state = state ?? ContactState()
        
        switch action {
            
        case is Actions.Contacts.Add:
            state.isLoading = true
            
        case let action as Actions.Contacts.Saved:
            state.items.append(action.contact)
            state.isLoading = false
            
        case let action as Actions.Contacts.Delete:
            state.items.removeAll { contact in
                contact.name == action.contact.name
            }
            
        default:
            break
        }
        
        return state
    }
}
