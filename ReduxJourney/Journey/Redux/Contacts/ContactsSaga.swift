//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift

extension Journey {
    
    struct ContactSaga: Saga {
        
        func actionReceived(_ action: Action, state: JourneyState, dispatch: DispatchFunction) async {
            switch action {
            case let action as Actions.Contacts.Add:
                await addContact(action, state, dispatch)
            default:
                break
            }
        }
        
        func addContact(_ action: Actions.Contacts.Add, _ state: JourneyState, _ dispatch: DispatchFunction) async {
            var contact = action.contact
            let useCase = state.configuration.useCase
            contact = await useCase.retrieveAmount(for: contact)
            contact = await useCase.registerContactInDatabase(contact)
            dispatch(Actions.Contacts.Saved(contact: contact))
        }
    }
}

