//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation

extension Actions {
    
    struct Contacts {
        
        struct Add: Action {
            let contact: Contact
        }
        
        struct Saved: Action {
            let contact: Contact
        }
        
        struct Delete: Action {
            let contact: Contact
        }
    }
}
