//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation
import SwiftUI

extension Journey {
    
    public struct Configuration: Equatable {
        
        let useCase: UseCase
        
        public init(useCase: UseCase = ContactUseCase()) {
            self.useCase  = useCase
        }
        
        public static func == (lhs: Configuration, rhs: Configuration) -> Bool {
            return true
        }
    }
}
