//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import ReSwift
import SwiftUI

public class Journey {
    
    public static func build(configuration: Journey.Configuration) -> AnyView {
        let saga = SagaMiddleware()
        let store = createStore(middleWare: saga.middlewareLayers, configuration: configuration)
        let viewModel = ViewModel(store: store)
        return AnyView(ListView(viewModel: viewModel))
    }
    
    static func createStore(middleWare: [Middleware<JourneyState>], configuration: Journey.Configuration) -> Store<JourneyState> {
        Store<JourneyState>(
            reducer: mainReducer,
            state: JourneyState(configuration: configuration),
            middleware: middleWare
        )
    }
}
