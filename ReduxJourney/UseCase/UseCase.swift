//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation

public protocol UseCase {
    
    func retrieveAmount(for contact: Contact) async -> Contact
    func registerContactInDatabase(_ contact: Contact) async -> Contact
}

public struct ContactUseCase: UseCase {
    
    public init () {}
    
    public func retrieveAmount(for contact: Contact) async -> Contact {
        await Task.sleep(1_000_000_000)
        var contact = contact
        contact.amount = "$5.00"
        return contact
    }
    
    public func registerContactInDatabase(_ contact: Contact) async -> Contact {
        await Task.sleep(1_000_000_000)
        // ...
        return contact
    }
}
