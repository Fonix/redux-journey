//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation

public struct Contact: Hashable {
    var name: String
    var email: String
    var amount: String
}
