//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    let journey = Journey.build(configuration: Journey.Configuration())
    
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    NavigationLink(destination: journey){
                        VStack {
                            Text("Go to journey")
                        }
                    }.navigationTitle("Redux")
                }
            }.navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
