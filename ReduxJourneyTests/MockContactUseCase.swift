//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import Foundation
@testable
import ReduxJourney

struct MockContactUseCase: UseCase {
    
    func retrieveAmount(for contact: Contact) async -> Contact {
        var contact = contact
        contact.amount = "$5.00"
        return contact
    }
    
    func registerContactInDatabase(_ contact: Contact) async -> Contact {
        // ...
        return contact
    }
}
