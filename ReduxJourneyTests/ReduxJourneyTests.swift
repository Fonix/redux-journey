//
// Copyright © 2021 Backbase R&D B.V. All rights reserved.
//

import XCTest
import ReSwift
@testable import ReduxJourney

class ReduxJourneyTests: XCTestCase {
    
    var store: Store<JourneyState>!
    var saga: Journey.SagaMiddleware!
    var viewModel: ViewModel!
    var view: ListView!
    let configuration = Journey.Configuration(useCase: MockContactUseCase())

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        saga = Journey.SagaMiddleware()
        store = Journey.createStore(middleWare: saga.middlewareLayers, configuration: configuration)
        saga.stateHistory = [store.state]
        viewModel = ViewModel(store: store)
        view = ListView(viewModel: viewModel)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // Arrange
        let contact = Contact(name: "Jason",
                              email: "Jason@email.com",
                              amount: "$5.00")
        
        var expectedState = JourneyState(configuration: configuration)
        expectedState.contacts.items = [contact]
        expectedState.contacts.isLoading = false
        
        // Act
        view.addContact(contact.name)
        
        // Assert
        XCTAssertEqual(expectedState, store.state)
    }
    
    func testExample2() throws {
        // Arrange
        let contact = Contact(name: "Jason",
                              email: "Jason@email.com",
                              amount: "$5.00")
        
        let expectedState = stateBuilder([
            {
                $0.contacts.isLoading = true
                return $0
            },
            {
                $0.contacts.isLoading = false
                $0.contacts.items = [contact]
                return $0
            }
        ])
        
        let expectedActions: [Action.Type] = [
            Actions.Contacts.Add.self,
            Actions.Contacts.Saved.self
        ]
        
        // Act
        view.addContact(contact.name)
        
        // Assert
        XCTAssertTrue(saga.stateHistory == expectedState)
        XCTAssertTrue(saga.actionHistory == expectedActions)
    }
}

func == (lhs: [Action], rhs: [Action.Type]) -> Bool {
    return lhs.elementsEqual(rhs, by: { action, actionType in
        return type(of: action.self) == actionType
    })
}

func == (lhs: [JourneyState], rhs: [JourneyState]) -> Bool {
    if lhs.count != rhs.count {
        XCTFail("Number of states does not match")
    }
    for (lhs, rhs) in zip(lhs, rhs) {
        if lhs != rhs {
            XCTFail("State data comparison was not equal")
        }
    }
    
    return true
}

func stateBuilder(states: [JourneyState] = [JourneyState(configuration: Journey.Configuration())],
                  index: Int = 0,
                  _ modifiers: [StateModifier]) -> [JourneyState] {
    
    if index == modifiers.count {
        return states
    }
    var states = states
    var state = states[index]
    states.append(modifiers[index](&state))
    
    return stateBuilder(states: states, index: index+1, modifiers)
}

typealias StateModifier = ((inout JourneyState) -> JourneyState)
